const express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),

    PORT = process.env.PORT || 5500,

    config = require('./config'),

    index = require('./routes/index'),
    products = require('./routes/products'),
    categories = require('./routes/categories'),
    uploads = require('./routes/uploads'),
    users = require('./routes/users'),
    app = express();

mongoose.Promise = global.Promise;
mongoose.connect(config.database, (err) => {
    if (err) throw err;
    console.log('DATABSE CONNECTED');
});

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use('/', index);
app.use('/categories', categories);
app.use('/products', products);
app.use('/uploads', uploads);
app.use('/users', users);

app.listen(PORT, () => {
    console.log('SERVER RUNNING ON PORT: ' + PORT);
});