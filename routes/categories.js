const router = require("express").Router(),
  { Category } = require("../models/model");

/** Get All Categories */
router.get("/", (req, res) => {
  Category.find()
    .exec()
    .then(categories => res.send(categories))
    .catch(err => {
      throw err;
    });
});

router.get("/:id", (req, res) => {
  Category.findOne({
    _id: req.params.id
  })
    .exec()
    .then(category => res.send(category))
    .catch(err => {
      throw err;
    });
});

/** Add Category */
router.post("/", (req, res) => {
  const category = new Category({
    name: req.body.name
  });
  category.save((err, category) => {
    if (err) {
      throw err;
    }
    res.json({
      success: true,
      category
    });
  });
});

router.put("/:id", (req, res) => {
  Category.findByIdAndUpdate(req.params.id, req.body)
    .then(category => res.send(category))
    .catch(err => {
      throw err;
    });
});

/** Delete Category */
router.delete("/:id", (req, res) => {
  Category.findOneAndRemove({
    _id: req.params.id
  })
    .exec()
    .then(category =>
      res.json({
        success: true
      })
    )
    .catch(err => {
      throw err;
    });
});

module.exports = router;
