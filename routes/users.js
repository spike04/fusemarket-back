const router = require('express').Router(),
    { User } = require('../models/model');

/** Get All Users */
router.get('/', (req, res) => {
    User.find()
        .exec()
        .then(users => res.send(users))
        .catch(err => { throw err });
})

/** Get One User */
router.post('/:userId', (req, res) => {
    User.findOne({ uuid: req.params.userId })
        .exec()
        .then(user => res.json({ success: true, user }))
        .catch(err => { throw err });
})

/** Register with Email and UUID */
router.post('/', (req, res) => {
    const user = new User({
        email: req.body.email,
        uuid: req.body.uuid
    });
    user.save((err, user) => {
        if (err) throw err;
        res.json({ success: true, user });
    })
})

module.exports = router;
