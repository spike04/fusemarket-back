const express = require('express'),
    Jimp = require('jimp'),
    router = express.Router();

router.get('/:image/:size', (req, res) => {
    const size = parseInt(req.params.size)
    const image = req.params.image
    const ok = (/\.(gif|jpg|jpeg|tiff|png)$/i).test(image)

    if (!ok) return res.send("Invalid Image Name")
    Jimp.read('uploads/products/' + image, (err, file) => {
        if (err) throw err;
        file
            .resize(size, Jimp.AUTO)
            .getBuffer(Jimp.AUTO, (err, file) => {
                res.writeHead(200, { 'Content-Type': 'image/jpg' });
                res.end(file);
            })
    })
});

module.exports = router