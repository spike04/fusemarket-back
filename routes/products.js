const router = require('express').Router(),
    {
        Product,
        Category
    } = require('../models/model'),
    multer = require('multer'),
    path = require('path'),
    fs = require('fs');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './uploads/products')
    },
    filename: (req, file, callback) => {
        callback(null, 'IMG_' + Date.now() + path.extname(file.originalname))
    }
});

const upload = multer({
    storage
}).single('file');

/** Get All Products */
router.get('/', (req, res) => {
    Product
        .find()
        .populate('category')
        .populate('userId', 'name email')
        .exec()
        .then(products => res.send(products))
        .catch(err => {
            throw err
        })
});

/** Search Functionality */
router.get('/search', (req, res) => {
    Product.find({
        $text: {
            $search: req.query.s
        }
    })
        .populate('category')
        .populate('userId', 'name email')
        .exec()
        .then(products => res.send(products))
        .catch(err => {
            throw err
        })
})

/** Get Products for Specified Category */
router.get('/category/:id', (req, res) => {
    Product.find({
        category: req.params.id
    })
        .populate('category')
        .populate('userId', 'name email')
        .exec()
        .then(products => res.send(products))
        .catch(err => {
            throw err
        })
});

/** Get All Products for the User */
router.get('/user/:id', (req, res) => {
    Product
        .find({
            userId: req.params.id
        })
        .populate('category')
        .exec()
        .then(products => res.send(products))
        .catch(err => {
            throw err
        })
});

router.get('/:id', (req, res) => {
    Product.findById(req.params.id)
        .populate('category')
        .exec()
        .then(product => res.send(product))
        .catch(err => {
            throw err
        })
})

/** Add Products */
router.post('/', (req, res) => {
    upload(req, res, err => {
        if (err) throw err;

        // const date = moment().utc(req.body.eventDate, "YYYY-MM-DD HH:mm:ss").toDate();
        // console.log(date);

        const product = new Product({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            negotiable: req.body.negotiable,
            productImage: req.file.filename,
            contact: req.body.contact,
            address: req.body.address,
            category: req.body.category,
            userId: req.body.userId
        });
        product.save((err, product) => {
            if (err) throw err;
            res.json({
                success: true,
                message: 'Product Added'
            });
        })
    });
});

/** Delete Category */
router.delete('/:id', (req, res) => {
    Product.findOneAndRemove({
        _id: req.params.id
    })
        .then(product => {
            fs.unlink('uploads/products/' + product.productImage, () => {
                res.json({
                    success: true
                });
            });
        })
        .catch(err => {
            throw err
        });
});

router.post('/:id', (req, res) => {
    if (!req.files) {
        // Here we will do a normal Update Operation
        const product = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            negotiable: req.body.negotiable,
            contact: req.body.contact,
            address: req.body.address,
            category: req.body.category,
            userId: req.body.userId
        };
        Product.findByIdAndUpdate(req.params.id, product)
            .populate('category')
            .populate('userId', 'name email')
            .exec()
            .then(product => res.send(product))
            .catch(err => {
                throw err
            })
    } else {
        // Here we need to delete the old Pic and add new image
        // Deleting old Image
        Product.findById(req.params.id)
            .exec()
            .then(product => {
                fs.unlink('./uploads/products/' + product.productImage, (err) => {
                    if (err) throw err;
                    upload(req, res, err => {
                        if (err) throw err;

                        const product = {
                            name: req.body.name,
                            description: req.body.description,
                            price: req.body.price,
                            negotiable: req.body.negotiable,
                            productImage: req.file.filename,
                            contact: req.body.contact,
                            address: req.body.address,
                            category: req.body.category,
                            userId: req.body.userId
                        };
                        Product.findByIdAndUpdate(req.params.id, product)
                            .populate('category')
                            .populate('userId', 'name email')
                            .exec()
                            .then(product => res.send(product))
                            .catch(err => {
                                throw err
                            })
                    });
                })
            })
            .catch(err => {
                throw err;
            })
    }
})

module.exports = router;