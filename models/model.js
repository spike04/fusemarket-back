const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

const Category = mongoose.model('Category', categorySchema);

const productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    price: {
        type: Number,
        required: true
    },
    negotiable: {
        type: String,
        default: 'yes'
    },
    productImage: {
        type: String,
        default: 'https://goo.gl/e3YfZX'
    },
    contact: {
        type: String,
        required: true
    },
    address: {
        type: String
    },
    category: {
        type: Schema.ObjectId,
        ref: 'Category'
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'User'
    }
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

productSchema.index({'name': 'text'});

const Product = mongoose.model('Product', productSchema);

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    uuid: {
        type: String,
        required: true
    },
    name: {
        type: String,
        default: ''
    },
    profileImage: {
        type: String,
        default: 'http://via.placeholder.com/150x150'
    },
    role: {
        type: String,
        default: 'user'
    }
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });

const User = mongoose.model('User', userSchema);

module.exports = {
    Category,
    Product,
    User
};